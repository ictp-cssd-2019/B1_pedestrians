import numpy as np
from scipy import spatial
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#Comment
def timestep():
    dots[:,:2] += dots[:,2:4] * h + 0.5 * dots[:,4:6] * h ** 2
    dots[:,2:4] += 0.5 * dots[:,4:6] * h
    collision()
    dots[:,2:4] += 0.5 * dots[:,4:6] * h
    dots[(dots[:,0] > size) | (dots[:,0] < 0) ,1] = None
    dots[(dots[:,1] > size) | (dots[:,1] < 0) ,0] %= size

def collision():
    dots[:,4:6] = 0
    '''
    Check if the other particle is within the radius R_cut for decaying repulsion force between particles
    '''

    distance_matrix = spatial.distance.cdist(dots[:,:2], dots[:,:2])
    distance_matrix = np.delete(distance_matrix, np.s_[num_particles:9 * num_particles], 0)
    ind1,ind2 = np.where(distance_matrix < R_cut)
    unique = (ind1 < ind2)
    ind1 = ind1[unique]
    ind2 = ind2[unique]

    for i, j in zip(ind1, ind2):
        r1x = dots[i, 0]
        r2x = dots[j, 0]
        r1y = dots[i, 1]
        r2y = dots[j, 1]
        cos_theta = (vdx * (r1x - r2x) + vdy * (r1y - r2y)) * ((vdx ** 2 + vdy ** 2) * ((r1x - r2x) ** 2 + (r1y - r2y) ** 2)) ** (-0.5)
        '''
        Check if the other particle is within the angle 80 degrees
        '''

        if cos_theta < 0.1736481777:
            r = distance_matrix[i,j]
            dots[j, 4] += A * np.exp(- r ** 2 / R ** 2) * (r1x - r2x) / r
            dots[j, 5] += A * np.exp(- r ** 2 / R ** 2) * (r1y - r2y) / r
            dots[i, 4] -= A * np.exp(- r ** 2 / R ** 2) * (r1x - r2x) / r
            dots[i, 5] -= A * np.exp(- r ** 2 / R ** 2) * (r1y - r2y) / r

def animate(i):
    timestep()
    #line.set_data(xs,ys)
    '''
    data = []
    for tuple in zip(xs,ys,colors):
        data.append(tuple[0])
        data.append(tuple[1])
        data.append('r' if tuple[2]==0 else 'b')
    plt.plot(*data, marker='o', markersize=8)
    '''
    plt.cla()
    ax.set_xlim(0,size)
    ax.set_ylim(0,size)
    plt.scatter(xs,ys,c=colors)

    return

num_particles = 10
size = num_particles
R_cut = size / 10
vdx = 0.5
vdy = 0
A = 1
R = 1
h = 0.1

dots = np.zeros((num_particles, 7), dtype = float)
dots[:, 6] = np.zeros(num_particles) + 1
dots[:, 0] = np.zeros(num_particles)
dots[:, 1] = np.random.rand(num_particles) * size
dots[:, 2] = vdx
dots[:, 3] = vdy

dots2 = np.zeros((num_particles, 7), dtype = float)
dots2[:, 6] = np.zeros(num_particles)
dots2[:, 0] = np.zeros(num_particles) + size
dots2[:, 1] = np.random.rand(num_particles) * size
dots2[:, 2] = -vdx
dots2[:, 3] = vdy

dots = np.concatenate((dots, dots2), axis = 0)
#animation
num_particles = 2 * num_particles

fig,ax = plt.subplots()
ax.set_xlim(0,size)
ax.set_ylim(0,size)
xs = dots[:,0]
ys = dots[:,1]

colors = dots[:,6]
#line, = ax.plot([],[],'b.',ms=15)
plt.scatter(xs,ys,c=colors)
ani = animation.FuncAnimation(fig,animate,interval=25,blit=False)
plt.show()
