Installation
=============

1. You need to install numpy and matplotlib library to install the simulation module:
    .. code-block:: python

        sudo apt-get install python3-tk
        pip install numpy matplotlib

2. Install the Pedestrian Dynamics Simulator Modules
    .. code-block:: python

        pip install -i https://test.pypi.org/simple/ pd-simulator-swoma

3. Import the *pd_simulator_pkg* and run the module in the following way:
    .. code-block:: python

         $ python
         >>> from pd_simulator_pkg import simulator as pds
         >>> pds.simulate()
         >>> pds.simulate(10,30) #total number of pedestrian in both groups