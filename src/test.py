import sys
sys.path.append('./pd_simulator_pkg')

from pedestrain import Pedestrain
from vector import Vector
import default_config

#Setting up Pedestrian to run experiment
vect_A = Vector(0,0)
vect_B = Vector(25,0)
vect_C = Vector(50,1)

pedestrian_A = Pedestrain(vect_A, vect_B, 1, 'b')
pedestrian_B = Pedestrain(vect_B, vect_A, 2, 'r')
pedestrian_C = Pedestrain(vect_C, vect_A, 3, 'y')

def test_pedestrian_interaction():
    '''
    Test if there is interaction between pedestrians
    '''
    #Get interaction space from class space
    fl_interaction = pedestrian_A.has_social_force_from(pedestrian_B)
    assert fl_interaction == True

def test_pedestrian_force_case_A():
    '''
    Test force between pedestrians horizontally aligned
    '''
    #Get interaction space from class space
    force = pedestrian_A.cal_social_force(pedestrian_B)

    assert force.x <= 0
    assert force.y == 0

def test_pedestrian_force_case_B():
    '''
    Test force between pedestrians pedestrian A
    '''
    #Get interaction space from class space
    force = pedestrian_A.cal_social_force(pedestrian_C)

    assert force.x <= 0
    assert force.y <= 0

def test_pedestrian_force_case_C():
    '''
    Test force between pedestrians pedestrian B
    '''
    #Get interaction space from class space
    force = pedestrian_C.cal_social_force(pedestrian_A)

    assert force.x <= 0
    assert force.y >= 0

def test_velocity_update():
    '''
    Test velocity update
    '''
    #Creating acceleration vector
    accel = Vector(2.5,0)
    vel = Vector(1.0,0.0)

    #Update velocity by acceleration
    up_vel = pedestrian_A.update_velocity(accel)

    assert up_vel.x == vect_A.x + accel.x/2.0 + vel.x
    assert up_vel.y == vect_A.y + accel.y/2.0 + vel.y
