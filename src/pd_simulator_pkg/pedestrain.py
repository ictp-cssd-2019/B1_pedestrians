from . import default_config
from .vector import Vector
import math

class Pedestrain(object):
    """
    Pedestrian module handles interaction between the pedestrians.
    This module provides a couple of helper methods to calculate the repulsive forces and thus helps to
    update the velocity and the updated position
    """

    def __init__(self, start, target, id, color):
        """
        Initialize the pedestrian object

        :param start: start position of pedestrian(type: Vector)
        :param target: intended target position of pedestrian(type: Vector)
        :param id: numeric id of the pedestrian
        :param color: type/color of the pedestrian
        """
        self.id = id
        self.color = color
        self.current_position = start
        self.target_position = target
        self.radius = default_config.DEFAULT_RADIUS
        self.mass = default_config.DEFAULT_MASS
        self.relaxation_time = default_config.DEFAULT_RELAXATION_TIME
        self.reach_goal = False
        self.acceleration = Vector(0,0)

        if(self.color=='purple'):
            self.actual_velocity = Vector(-1,0)
            self.desired_speed = -1.0*default_config.DEFAULT_DESIRED_SPEED
        else:
            self.actual_velocity = Vector(1, 0)
            self.desired_speed = default_config.DEFAULT_DESIRED_SPEED

        self.desired_velocity = Vector(self.desired_speed,0)

    def cal_friction(self):
        return (self.desired_velocity.subtract(self.actual_velocity)).divide_by(self.relaxation_time)


    def cal_social_force(self, neighbour):
        """
        calculate the social force generate from the neighbour pedestrian

        :param neighbour: neighbour Pedestrian object
        :return: social force generated from the neighbour pedestrian
        """
        A = default_config.A
        R = default_config.R_FORCE_TERRITORY

        diff = self.current_position.subtract(neighbour.current_position)
        diff_length = diff.get_length()
        force_multiplier = A * math.exp(-1.0*diff_length*diff_length/(R*R)) * self.has_social_force_from(neighbour)
        diff_normalize = diff
        if(diff_length!=0.):
            diff_normalize = diff.divide_by(diff_length)
        social_force = diff_normalize.multiply_by(force_multiplier)
        return social_force

    def has_social_force_from(self, neighbour):
        """
        It's a step function which will return 1 if the neighbour is near to the pedestrian, otherwise 0

        :param neighbour: neighbour Pedestrian object
        :return: either 1.0 or 0.0 based on the neighbour position
        """
        diff = self.current_position.subtract(neighbour.current_position)
        diff_length = diff.get_length()
        if (diff_length == 0):
            diff_length = 0.01
        dot_product = diff.dot_product(self.desired_velocity)
        angle = math.acos(-dot_product/(diff_length*self.desired_velocity.get_length()))
        return 1.0 if (abs(angle) < (80.0 * math.pi / 180.0)) else 0.0

        dot_product = diff.dot_product(self.actual_velocity)
        if(diff_length!=0 and self.desired_velocity.get_length()!=0):
            angle = dot_product/(diff_length*self.desired_velocity.get_length())
        else:
            angle = dot_product

        return 1.0 if angle<math.cos(80.0*math.pi/180.0) else 0.0

    def update_velocity(self, new_acceleration):
        """

        :param new_acceleration: updated acceleration calculated based on the neighbour pedestrian repulsive force
        :return: updated velocity
        """
        self.actual_velocity = self.actual_velocity.add(self.acceleration.divide_by(2.0))
        self.actual_velocity = self.actual_velocity.add(new_acceleration.divide_by(2.0))
        return self.actual_velocity

    def update_acceleration(self, acceleration):
        self.acceleration = acceleration

    def set_actual_velocity(self,velocity):
        self.actual_velocity = velocity

    def update_position(self):
        """
        update the position based on the repulsive force
        """
        self.current_position = self.current_position.add(self.actual_velocity)
        self.current_position = self.current_position.add(self.acceleration.divide_by(2.0))
        self.check_reach_goal()

    def check_reach_goal(self):
        """
        This method check whether the pedestrian is reach is target
        :return: True or False
        """
        if( self.color=='peru' and self.current_position.x>self.target_position.x):
            self.current_position.x %= default_config.HALL_LENGTH
        elif( self.color=='purple' and self.current_position.x<self.target_position.x):
            self.current_position.x = (self.current_position.x) % default_config.HALL_LENGTH

        if(self.reach_goal):
            print("Particle {} reach it's goal".format(self.id))
            #print(self)

    def __str__(self):
        particle_info ="""Particle Id: {}
Type: {}
Current Position: {}
Target: {}
Reach Goal: {}
Current Velocity: {}
Desired Velocity: {}
Mass: {}
Radius: {}""".format(self.id,
                     self.color,
                     str(self.current_position),
                     str(self.target_position),
                     self.reach_goal,
                     self.actual_velocity,
                     self.desired_velocity,
                     self.mass,
                     self.radius)

        return particle_info
