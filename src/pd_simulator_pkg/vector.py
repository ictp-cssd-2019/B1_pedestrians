import math
import numpy as np

class Vector(object):
    """
    This Vector provide some helper methods for basic vector operation
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def calculate_distance_from(self, vector):
        """
        Calculate the distance between two vector
        :param vector: Vector object
        :return: distance between two vectors
        """
        diff_x = self.x - vector.x
        diff_y = self.y - vector.y

        return math.sqrt((diff_x * diff_x) + (diff_y * diff_y))

    def get_length(self):
        """
        calculate the modulus of a vector
        """
        return math.sqrt(self.x*self.x+self.y*self.y)

    def normalize_by(self, vector):
        """

        :param vector: Vector Object
        :return: return the direction between two vector
        """
        distance = self.calculate_distance_from(vector)
        diff_x = self.x - vector.x
        diff_y = self.y - vector.y

        if(distance!=0):
            diff_x /= distance
            diff_y /= distance

        return Vector(diff_x,diff_y)

    def cal_angle(self, vector):
        """
        calculate angle between two vectors
        :param vector: Vector Object
        :return: angle in degrees
        """
        v0 = np.array([self.x,self.y])
        v1 = np.array([vector.x, vector.y])
        angle = np.math.atan2(np.linalg.det([v0, v1]), np.dot(v0, v1))
        return np.degrees(angle)

    def multiply_by(self, val):
        return Vector(self.x * val, self.y * val)

    def divide_by(self, val):
        return Vector(self.x / val, self.y / val)

    def add(self, vector):
        return Vector(self.x + vector.x, self.y + vector.y)

    def subtract(self, vector):
        return Vector(self.x - vector.x, self.y - vector.y)

    def dot_product(self, vector):
        return self.x * vector.x + self.y*vector.y

    def __str__(self):
        return "({},{})".format(self.x, self.y)
