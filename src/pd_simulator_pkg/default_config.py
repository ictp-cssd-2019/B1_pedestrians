"""
This module contains all the configuration of simulation environment
"""


# Animation Config
DOT_MARKER_SIZE = 8
ANIMATION_INTERVAL = 1 #animation interval in seconds

# Particle interaction config

DEFAULT_RADIUS = 4.0
DEFAULT_MASS = 6.0
DEFAULT_RELAXATION_TIME = 2.5
DEFAULT_DESIRED_SPEED = 0.5
DEFAULT_MAX_SPEED = 3.0
R_FORCE_TERRITORY = 5.0
A = 1.0
HALL_LENGTH = 100
HALL_HEIGHT = 20
TOTAL_PEDESTRIAN_G1 = 40
TOTAL_PEDESTRIAN_G2 = 60
