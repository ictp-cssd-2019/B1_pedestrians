
from .pedestrain import Pedestrain
from .vector import Vector
from . import default_config
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random
import json


class Simulator(object):
    """
    Simulator class is responsible for simulating the pedestrian dynamics

    *  it creates the environment either with the default configuration or it can use configuration file(if available)
    *  update the pedestrian position based on it's interaction with the other pedestrian or obstacle
    *  plot/animate the pedestrian movements

    :parameter
    ----------

    :param total_pedestrains: total number of pedestrian in the simulation environment
    :param env_start_position: lower left corner position(x,y) of the simulation environment
    :param env_start_position: upper right corner position(x,y) of the simulation environment
    """

    def __init__(self, total_pedestrains_g1, total_pedestrains_g2, env_start_position, env_end_position):
        self.pedestrains = []
        self.total_pedestrains_g1 = total_pedestrains_g1
        self.total_pedestrains_g2 = total_pedestrains_g2
        self.env_start_position = env_start_position
        self.env_end_position = env_end_position
        self.gen_pedestrains(total_pedestrains_g1,total_pedestrains_g2)

    def gen_pedestrains(self, total_pedestrains_g1,total_pedestrains_g2):
        """
         gen_pedestrians method generate a set of of pedestrians for the simulation

        :param total_pedestrains_g1: total number of pedestrians in the first group (pedestrians who are going from the left to right)
        :param total_pedestrains_g2: total number of pedestrians in the second group (pedestrians who are going from the right to left)
        """

        target_position = Vector(self.env_end_position.x,self.env_end_position.y//2)
        rev_target_position = Vector(self.env_start_position.x,self.env_end_position.y//2)
        pedestrain_id = 1
        for i in range(0,total_pedestrains_g1):
            startpoint_y = random.randint(self.env_start_position.y, self.env_end_position.y)
            startpoint_x = random.randint(self.env_start_position.x, self.env_end_position.x)
            start_position = Vector(startpoint_x,startpoint_y)
            self.pedestrains.append(Pedestrain(start_position, target_position, pedestrain_id, 'peru'))
            pedestrain_id += 1

        for i in range(0,total_pedestrains_g2):
            startpoint_y = random.randint(self.env_start_position.y, self.env_end_position.y)
            startpoint_x = random.randint(self.env_start_position.x, self.env_end_position.x)
            start_position = Vector(startpoint_x, startpoint_y)
            self.pedestrains.append(Pedestrain(start_position, rev_target_position, pedestrain_id, 'purple'))
            pedestrain_id += 1

    def simulate(self):
        """
        simulate() method helps to simulate and visualize the pedestrian dynamics

        """
        fig, ax = plt.subplots(figsize=(10,4))

        ax.set_xlim(0, self.env_end_position.x)
        ax.set_ylim(0, self.env_end_position.y)

        def init():
            return []

        def animate(frame_number):
            self.update_velocity()
            data = []
            # print("-----------------")

            for pedestrain in self.pedestrains:
                # print(pedestrain)
                data.append(pedestrain.current_position.x)
                data.append(pedestrain.current_position.y)
                data.append(pedestrain.color)

            animlist = plt.plot(*data,marker='o',markersize=default_config.DOT_MARKER_SIZE)
            return animlist

        ani = animation.FuncAnimation(fig, animate,
                                      interval=default_config.ANIMATION_INTERVAL,
                                      blit=True)
        #ani.save('2pedestrians.mp4', fps=5000, extra_args=['-vcodec', 'libx264'])
        plt.show()

    def update_velocity(self):
        """
        update_velocity() method periodically update the pedestrian velocity, acceleration
        as well as the position based on the repulsive force generated from the neighbour pedestrian
        :return:
        """

        total_pedestrains = len(self.pedestrains)
        for i in range(0, total_pedestrains):
            if(self.pedestrains[i].reach_goal): continue

            new_acceleration = self.pedestrains[i].cal_friction()
            for j in range(0, total_pedestrains):
                if (i == j or self.pedestrains[j].reach_goal): continue
                new_acceleration = new_acceleration.add(self.pedestrains[i].cal_social_force(self.pedestrains[j]))
            new_velocity = self.pedestrains[i].update_velocity(new_acceleration)

            self.pedestrains[i].set_actual_velocity(new_velocity)
            self.pedestrains[i].update_acceleration(new_acceleration)

        for pedestrian in self.pedestrains:
            pedestrian.update_position()

def load_configuration(config_filename):
    config = ""
    with open(config_filename) as config_data:
        config = json.load(config_data)
    
    return config

def simulate(total_pedestrains_g1=None,total_pedestrains_g2=None,hall_width=None,hall_height=None):
    """
    simulate the pedestrian interaction

    :param total_pedestrains_g1: total number of pedestrians in the first group (pedestrians who are going from the left to right), otherwise default value load from configuration
    :param total_pedestrains_g2: total number of pedestrians in the second group (pedestrians who are going from the right to left), otherwise default value load from configuration
    :param hall_width: hall length, otherwise default value load from configuration
    :param hall_height: hall width, otherwise default value load from configuration
    :return:
    """
    config = load_configuration('simulation_env.config')
    if(config==""):
        print("Configuration file is missing")
        print("Loading default configuration")
    else:
        print("Loading configuration from the simulation_env_config file")
        default_config.HALL_LENGTH = config['simulation_env_conf']['parameters']['hall_width']
        default_config.HALL_HEIGHT = config['simulation_env_conf']['parameters']['hall_height']

        default_config.R_FORCE_TERRITORY = config['simulation_env_conf']['parameters']['R']
        default_config.DEFAULT_DESIRED_SPEED = config['simulation_env_conf']['parameters']['desired_speed']
        default_config.A = config['simulation_env_conf']['parameters']['A']
        default_config.TOTAL_PEDESTRIAN_G1 = config['simulation_env_conf']['parameters']['total_pedestrian_group_one']
        default_config.TOTAL_PEDESTRIAN_G2 = config['simulation_env_conf']['parameters']['total_pedestrian_group_one']


    if (total_pedestrains_g1 == None):
        total_pedestrains_g1 = default_config.TOTAL_PEDESTRIAN_G1
    if (total_pedestrains_g2 == None):
        total_pedestrains_g2 = default_config.TOTAL_PEDESTRIAN_G2
    if (hall_width != None):
        default_config.HALL_LENGTH = hall_width
    if (hall_width != None):
        default_config.HALL_HEIGHT = hall_height

    simulator_env_start_position = Vector(0,0)
    simulator_env_end_position = Vector(default_config.HALL_LENGTH,20)

    
    simulator = Simulator(total_pedestrains_g1, total_pedestrains_g2, simulator_env_start_position, simulator_env_end_position)
    simulator.simulate()

def main():
    simulate()
    
if __name__ == "__main__":
    main()
