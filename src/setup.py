import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pd_simulator_swoma",
    version="0.0.6",
    author="SWOMA",
    author_email="info@swoma.com",
    description="This package will help to simulate the pedestrian movement simualtion.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ictp-cssd-2019/B1_pedestrians",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
