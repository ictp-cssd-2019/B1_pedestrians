# Pedestrian Dynamics Simulator

In this project we develop a simulator to simulate the pedestrian 
dynamics in emergency situation.

[**Project Description**](https://drive.google.com/file/d/14KwxJtamW_ohebs-wBfBA40WB3OsYeG_/view?usp=sharing)

[**Project Presentation**](https://docs.google.com/presentation/d/1ms5IDBeWyKOUoIgnc_m8UDUttottXI8gjGJK93zwbv0)

[**Installation Instruction**](https://ictp-cssd-2019.gitlab.io/B1_pedestrians/installation.html)

[**Documentation**](https://ictp-cssd-2019.gitlab.io/B1_pedestrians/)

[**Video Demo**](https://youtu.be/22nEp8Gvnkg)

Dependencies:
* numpy (>=1.16)
* matplotlib (>=3.0.3)

## Group Members

* Foroozan Setareh (Physics implementation - Code)
* Garcia Herrera William Javier (Project manager - Unit tests)
* Hanafy Osama Sayed (Documentation)
* Islam(Akash) Md Mofijul (Technical lead - Code - Packaging)
* Lahdour Mohamed Mohamed (Unit tests - Configuration simulation)



## License

This code is distributed under the MIT license, see the LICENSE file.
